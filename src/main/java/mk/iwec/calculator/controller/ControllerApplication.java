package mk.iwec.calculator.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import mk.iwec.calculator.controller.services.CalculatorService;

@SpringBootApplication
@ComponentScan(basePackages = {"mk.iwec.rest.controllers"})
public class ControllerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ControllerApplication.class, args);
		
	}

}
