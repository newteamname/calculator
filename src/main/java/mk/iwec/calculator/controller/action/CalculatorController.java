package mk.iwec.calculator.controller.action;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mk.iwec.calculator.controller.services.CalculatorService;

@RestController
@RequestMapping("api/calculator")
public class CalculatorController {

	@Autowired
	CalculatorService calc;

	@GetMapping("/add")
	public Integer add(@RequestParam(name = "firstDigit") Integer a, @RequestParam(name = "secondDigit") Integer b) {
		return calc.add(a, b);
	}
	
	@GetMapping("/divide")
	public Integer divide(@RequestParam(name = "firstDigit") Integer a, @RequestParam(name = "secondDigit") Integer b) {
		return calc.divide(a, b);
	}
	
	@GetMapping("/multiply")
	public Integer multiply(@RequestParam(name = "firstDigit") Integer a, @RequestParam(name = "secondDigit") Integer b) {
		return calc.multiply(a, b);
	}

}
