package mk.iwec.calculator.controller.services;

public class CalculatorServiceImpl implements CalculatorService {

	public Integer multiply(Integer a, Integer b) {
		return a*b;
	}
	
	public Integer divide(Integer a, Integer b) {
		return a/b;
	}
	
	public Integer add(Integer a, Integer b) {
		return a+b;
	}
}
