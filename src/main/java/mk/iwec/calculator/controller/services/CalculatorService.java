package mk.iwec.calculator.controller.services;

public interface CalculatorService {
	
	public Integer multiply(Integer a, Integer b);
	
	public Integer divide(Integer a, Integer b);
	
	public Integer add(Integer a, Integer b);

}
